//Isaac Sotelo

#include <iostream>
#include <string>

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx = 0, float yy = 0, float zz = 0) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 add(const Vector3& v, const Vector3& v2) { // v and v2 are copies, so any changes to them in this function
                                     // won't affect the originals
    
	//add vectors component-wise during instantiation
    Vector3 result(v.x+v2.x,v.y+v2.y,v.z+v2.z);
    return result;
}

Vector3 operator+(const Vector3& v, const Vector3& v2){
	return add(v,v2);
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector
    stream << v.x << " " << v.y << " " << v.z; 

    return stream;
}

int main(int argc, char** argv){
//Part 9 References 

Vector3 v(1,2,3); 
Vector3 v2(4,5,6);

Vector3 v3 = v + v2;

std::cout << v3 << std::endl;

//=======================================
//Part 8a Arrays

//array on the stack
// Vector3 array[10];
//=======================================
//Part 8b Arrays

//array on the heap
//  Vector3* array2 = new Vector3[10];
//  for (int i = 0; i < 10; i++){
//  	array2[i].y = 5;
//  }
//  for (int i = 0; i < 10; i++){
//  std::cout << array2[i] << std::endl;
// }
//=======================================

// Vector3 q(0,0,0);
// q.y = 5;
// std::cout << q << std::endl;
//=======================================

// Vector3 v(1,2,3); 
// Vector3 v2(4,5,6);
// std::cout << v+v2 << std::endl;

//=======================================
// Vector3 a(1,2,3); 
// Vector3 b(4,5,6);
// Vector3 c = a+b;

//print vector C result
// std::cout << "(" << c.x << ", " << c.y << ", " << c.z << ")"<< std::endl;

//=======================================
//printing command-line arguments	
// for (int i = 0; i < argc; ++i){
// 	std::cout << argv[i] << std::endl;
// }

//=======================================
// //getting input from command line
// std::cout << "Name?" << std::endl;
// std::string name;
// std::cin >> name;
// // std::cout << "hello " <<  name << " &lt;what the user types&gt;" << std::endl;
// std::cout << "hello &lt;what the user types &gt;" << std::endl;

}

struct vec3 {
    float x;
    float y;
    float z;

    float lengthSquared() {
        return x*x+y*y+z*z;
    }
};

class Foo {
private:
    float a; // only available to methods of Foo
    int b;
    char c;

    void bar();

protected:
    float d; // only available to methods of Foo or objects that inherit from Foo
    int e;
    char f;

    void blah();

public:
    float g; // available anywhere
    int h;
    char i;

    void foo();
};

